# Antv-G6 学习资料

`Antv-G6` 学习资料
本项目分别在 `Vue3` 和 `React18` 中使用 `Antv-G6`。

在学习可视化框架时我抱着这样一个想法：看文档不如看 `demo` 。
于是就有了本项目。

# 参考资料
[图文笔记](https://juejin.cn/user/2673620576140030/posts)